#!/usr/bin/env python
#-*- coding=UTF-8 -*-
from selenium import webdriver
from selenium.webdriver.common.by import By
from selenium.common.exceptions import NoSuchElementException
from selenium.webdriver.support.select import Select
from selenium.webdriver.common.action_chains import ActionChains
from selenium.webdriver.common.alert import Alert


from time import sleep

from Method import output,verify


class sign_up(object):

    def signup(self,browser,url,email,username,passwd):
        '''pass:signup a account
        '''
        output.openfile('test')
        output.save(u'test_sign_up')

        self.driver = browser()
        self.driver.get(url)

        E_mail = self.driver.find_element_by_id('signup-email')
        E_mail.send_keys(email)

        user_name = self.driver.find_element_by_id('signup-username')
        user_name.send_keys(username)

        pass_word = self.driver.find_element_by_id('signup-password')
        pass_word.send_keys(passwd)

        pass_word_confirm = self.driver.find_element_by_id('signup-password-confirm')
        pass_word_confirm.send_keys(passwd)

        submit_button = self.driver.find_element_by_id('signup-submit')
        submit_button.submit()

        v = verify.v_element(self.driver,By.LINK_TEXT,u"个人中心")
        if v:
            output.save('Pass')
        else:
            output.save('Fail')

    def logout(self):
        '''pass:log out KKG
        '''
        output.save('test_sign_up_logout')
        self.driver.find_element_by_link_text(u"退出").click()
        sleep(10)
        v = verify.v_element(self.driver,By.LINK_TEXT,u"登录")
        if v:
            output.save('Pass')
        else:
            output.save('Fail')
        self.driver.quit()

class member_section(object):
    '''the test of member
    '''

    def login (self,browser,url,username,passwd):
        '''pass:Log in KKG
        '''
        output.openfile('test')
        output.save('test_login')

        self.driver = browser()
        self.driver.get(url)

        user_name = self.driver.find_element_by_id('user')
        user_name.send_keys(username)

        pass_word = self.driver.find_element_by_id('psd')
        pass_word.send_keys(passwd)

        submit_button = self.driver.find_element_by_id('loginBtn')
        submit_button.submit()

        v = verify.v_element(self.driver,By.LINK_TEXT,u'个人中心')
        if v:
            output.save('Pass')
        else:
            output.save('Fail')



    def buy_services(self,url):
        '''
            pass: buy a servce (Only create an order)
        '''
        self.driver.get(url)


        #self.driver.find_element_by_class_name('confirm_btn_b').submit()
        self.driver.find_element_by_xpath("//div[contains(@class,'apeci_tab')]/a[1]").click()
        self.driver.find_element_by_class_name('confirm_btn_b').submit()

        self.driver.find_element_by_id('buyPhone').send_keys('13717607755')
        sleep(5)
        self.driver.find_element_by_css_selector('button.confirm_btn_b.mr20').submit()

    def buy_product(self,url):
        '''pass: Buy a product (Only create an order)
        '''
        self.driver.get(url)


        #self.driver.find_element_by_class_name('confirm_btn_b').submit()
        self.driver.find_element_by_xpath("//div[contains(@class,'apeci_tab')]/a[1]").click()

        self.driver.find_element_by_xpath("//div[contains(@class,'speci_line')]/a[1]").click()

        self.driver.find_element_by_class_name('add_btn').click()         
        
        self.driver.find_element_by_class_name('confirm_btn_b').submit()

        comment = self.driver.find_element_by_xpath("//div[contains(@class,'leave_msg')]/input")

        comment.send_keys(u'hello please quickly')
        try:
            self.driver.find_element_by_xpath("//div[contains(@class,'address_one')]/span/a").click()

            self.driver.find_element_by_id('conmitOrder').submit()
        except NoSuchElementException,e:
            self.driver.find_element_by_xpath("//div[contains(@class,'write_address')]/div/dl/dd/input").send_keys('tester')

            province = self.driver.find_element_by_id('province')
            Select(province).select_by_value(u'北京市')

            area = self.driver.find_element_by_id('area')
            Select(area).select_by_value(u'市辖区')

            city = self.driver.find_element_by_id('city')
            Select(city).select_by_value(u'通州区')

            self.driver.find_element_by_xpath("//input[contains(@name,'address[street]')]").send_keys('Completely address')

            self.driver.find_element_by_xpath("//input[contains(@name,'address[zipcode]')]").send_keys('063000')

            self.driver.find_element_by_id("takePhone").send_keys('13717607755')

            self.driver.find_element_by_id('conmitOrder').submit()





    def add_addr(self,name,province,city,area,street,zipcode,mobile):
        '''pass : Add the address for receiving product 
        '''
        self.driver.find_element_by_link_text(u'个人中心').click()
        sleep(2)
        self.driver.find_element_by_xpath("//dl[contains(@class,'secondary')]/dd/a[3]").click()
        sleep(2)
        self.driver.find_element_by_xpath("//a[contains(@id,'addNewsAddress')]").click()

        #new_add = self.driver.find_element_by_id('addNewsAddress')
        #self.driver.refresh()
        #ActionChains(self.driver).double_click(new_add).perform()

        new_name = self.driver.find_element_by_name(u'name')
        ActionChains(self.driver).move_to_element(new_name).send_keys(name).perform()

        s1 = self.driver.find_element_by_id(u'province')
        Select(s1).select_by_value(province)
        s2 = self.driver.find_element_by_id(u'area')
        Select(s2).select_by_value(city)
        s3 = self.driver.find_element_by_id(u'city')
        Select(s3).select_by_value(area)

        self.driver.find_element_by_name('street').send_keys(street)
        self.driver.find_element_by_name('zipcode').send_keys(zipcode)
        self.driver.find_element_by_name('mobile').send_keys(mobile)

        self.driver.find_element_by_css_selector('button.confirm_btn_b.mt15').submit()

        #self.driver.find_element_by_xpath("//button[contains(@class,'confirm_btn_b)] and [contains(@class,'mt15)]").click()
        #class_name('confirm_btn_b mt15').submit()

    def modify_addr(self,name,province,city,area,street,zipcode,mobile):
        '''finish and no pass : Modify the address
        '''
        self.driver.find_element_by_link_text(u'个人中心').click()
        sleep(2)
        self.driver.find_element_by_xpath("//dl[contains(@class,'secondary')]/dd/a[3]").click()
        sleep(2)
        self.driver.find_element_by_link_text('修改').click()
        sleep(2)

        new_name = self.driver.find_element_by_name(u'name') #??????????????????????????????????????????????????????????????
        ActionChains(self.driver).move_to_element(new_name).send_keys(name).perform()

        s1 = self.driver.find_element_by_id(u'province')
        Select(s1).select_by_value(province)
        s2 = self.driver.find_element_by_id(u'area')
        Select(s2).select_by_value(city)
        s3 = self.driver.find_element_by_id(u'city')
        Select(s3).select_by_value(area)

        self.driver.find_element_by_name('street').send_keys(street)     #??????????????????????????????????????????????????????????????????????????????????
        self.driver.find_element_by_name('zipcode').send_keys(zipcode)   #????????????????????????????????????????????????????????????????????????????????
        self.driver.find_element_by_name('mobile').send_keys(mobile)     #????????????????????????????????????????????????????????????????????????????????

        self.driver.find_element_by_css_selector('button.confirm_btn_b.mt15').submit()

    def del_addr(self):
        '''finish no pass : delete the address
        '''
        self.driver.find_element_by_link_text(u'个人中心').click()
        sleep(2)
        self.driver.find_element_by_xpath("//dl[contains(@class,'secondary')]/dd/a[3]").click()
        sleep(2)
        self.driver.find_element_by_link_text('删除').click()#????????????????????????????????????????????????????????????????????????????
        self.driver.switch_to_alert()
        Alert(self.driver).accept()

    def no_del_addr(self):
        '''finish no pass: cancel delete the adddress
        '''
        self.driver.find_element_by_link_text(u'个人中心').click()
        sleep(2)
        self.driver.find_element_by_xpath("//dl[contains(@class,'secondary')]/dd/a[3]").click()
        sleep(2)
        self.driver.find_element_by_link_text('删除').click() #????????????????????????????????????????????????????????????????????????????????????????
        self.driver.switch_to_alert()
        Alert(self.driver).dismiss()

        
    def modify_passwd(self,old_pd,new_pd):
        '''no pass : modify password
        '''
        self.driver.find_element_by_id('oldPsd').send_keys(old_pd)
        self.driver.find_element_by_id('newPsd').send_keys(old_pd)
        self.driver.find_element_by_id('repeatPsd').send_keys(old_pd)
        self.driver.find_element_by_xpath("//dl[contains(@class,'reset_text')]/dd/button").submit()

    def modify_basic_data(self,path_portrait,sex,phone,qq,area):
        '''finish and no pass : modify the information of the member 
        '''
        self.driver.find_element_by_link_text(u'个人中心').click()
        sleep(2)
        self.driver.find_element_by_link_text(u"基本资料]").click()

        self.driver.find_element_by_name('upload_image').send_keys(path_portrait) #???????????????????????????????????????????????

        sel1 = self.driver.find_element_by_class_name('f-city')
        Select(sel).select_by_value(sex)

        self.driver.find_element_by_name('mobile').send_keys(phone)

        self.driver.find_element_by_name('qq').send_keys(qq)

        sel2 = self.driver.find_element_by_name('city_id')
        Select(sel2).select_by_value(area)

        self.driver.find_element_by_xpath("//dl[contains(@class,'reset_text')]/dd/button").submit()

    def search_by_text(self,content,url):
        '''pass:search via search text
        '''
        self.driver.get(url)

        search = self.driver.find_element_by_xpath ("//div[contains(@class,'search_box')]/form/fieldset/input")
        content.encode('UTF-8')
        search.send_keys(content)

        search_button = self.driver.find_element_by_xpath("//div[contains(@class,'search_box')]/form/fieldset/button")
        search_button.submit()
    

    def search_by_keyword(self,kw,url):
        '''pass:search via key word
        '''
        self.driver.get(url)
        hotk.encode('UTF-8')
        search = self.driver.find_element_by_link_text(kw)
        search.click()



    def search_by_link(self,lk,url):
        '''No pass : Search via by Link 
        '''
        self.driver.get(url)

        search = self.driver.find_element_by_link_text(lk)
        search.clikc()

    def logout(self):
        '''log out the KKG
        '''
        output.save('test_login_logout')
        self.driver.find_element_by_link_text(u"退出").click()

        sleep(10)
        v = verify.v_element(self.driver,By.LINK_TEXT,u"登录")
        if v:
            output.save('Pass')
        else:
            output.save('Fail')

        
        self.driver.quit()
        output.quit()

        #try:self.driver.find_element (By.LINK_TEXT,u'登陆')
        #except NoSuchElementException, e:output.save('Faile')
        #output.save('Pass')


class supplier_section (object):
    '''The test of Supply.create the product ; check the orders ; modify the information of the Supply.
    '''
    def login (self,browser,url,username,passwd):
        '''finish not pass: log in the KKG via Supply
        '''
        self.driver = browser()
        self.driver.get(url)
        self.driver.find_element_by_id('biz-username').send_keys(username)
        self.driver.find_element_by_id('biz-password').send_keys(passwd)
        self.driver.find_element_by_id('login-submit').submit()

    
    #d = {'url':'http://testzuitu.hbldiy.com/biz/team/sedit.php','category':'2' , 'nameofporduct':u'商品名称' , 'color':u'红色', 'model':u'XL', 'royalties':50 , 'pathofpic':r'C:\Users\clg\Desktop\UI\red.png', \
    #'introductofproduct':'hello', 'mend':True, 'after_sale':'after sale_information' ,'saletype':'immediate', 'postname':'shunfeng', 'postprice':0.5}


    def create_product(self,values):
        '''no pass and finish : create product
        '''
        self.driver.get(values['url'])
        
        #u'枚举合适测试''
        sel = self.driver.find_element_by_id('subId')
        Select(sel).select_by_value(values['category']) #2=u'音响设备'，3=u'运动器材'，4=u'营养健康'，130=u'音响设备'
        self.driver.find_element_by_id('addCate').click()

        #u'商品名称、color and size'
        self.driver.find_element_by_name('title').send_keys(values['nameofporduct'])
        self.driver.find_element_by_xpath("//div[contains(@id,'brand')]/div/a").click()
        self.driver.find_element_by_id('shopColour').send_keys(values['color'])
        self.driver.find_element_by_id('shopModel').send_keys(values['model'])
        
        #set size and color
        i = len(values['color'].split(','))
        j = 3
        k = len(values['model'].split(','))
        for a in range(1,i+1,1):
            for b in range (3,6,1):
                for c in range (1,k+1,1):
                    self.driver.find_element_by_xpath("//tbody/tr[%d]/td[%d]/input[%d]" %(a,b,c))

        '''
        self.driver.find_element_by_xpath("//tbody/tr[1]/td[3]/input[1]")
        self.driver.find_element_by_xpath("//tbody/tr[1]/td[3]/input[2]")

        self.driver.find_element_by_xpath("//tbody/tr[1]/td[4]/input[1]")
        self.driver.find_element_by_xpath("//tbody/tr[1]/td[4]/input[2]")

        self.driver.find_element_by_xpath("//tbody/tr[1]/td[5]/input[1]")
        self.driver.find_element_by_xpath("//tbody/tr[1]/td[5]/input[2]")

        self.driver.find_element_by_xpath("//tbody/tr/td[3]/input[1]")
        self.driver.find_element_by_xpath("//tbody/tr/td[3]/input[2]")
        '''

        self.driver.find_element_by_class_name('input_text').send_keys(values['royalties'])  #???????????????????????????????????????????????????????????????????????????????????????????????????

        self.driver.find_element_by_name('upload_image').send_keys(values['pathofpic'])  #???????????????????????????????????????????????????????????????

        self.driver.find_element_by_class_name('ke-content').send_keys(values['introductofproduct'])

        #u'保修与否'
        if value['mend']:
            self.driver.find_element_by_xpath("//dd[contains(@class,radioBox)]/div[1]/a")
        else:
            self.driver.find_element_by_id("//dd[contains(@class,radioBox)]/div[2]/a")


        #u'售后说明'
        self.driver.find_element_by_class_name('s_datail_textarea').send_keys(values['after_sale'])

        #u'快递名称'
        self.driver.find_element_by_name('express_name').send_keys(values['postname'])

        #u'价格'
        self.driver.find_element_by_name('express_price').send_keys(values['postprice'])

        #button (submit)
        self.driver.find_element_by_class_name('confirm_btn_b').submit()

    def modify_password(self,service_code,oldpd,pd1,pd2):
        '''no pass and finish : 打开供应商中心
        '''
        self.driver.find_element_by_link_text(u'供应商中心').click()

        self.driver.find_element_by_link_text(u'供应商信息').click()

        self.driver.find_element_by_name('oldpassword').send_keys(oldpd)
        self.driver.find_element_by_name('password').send_keys(pd1)
        self.driver.find_element_by_name('password2').send_keys(pd2)

        self.driver.find_element_by_class_name('confirm_btn_b').submit()

    def modify_supply_introduce(self,contain):
        '''no pass and finish :打开供应商中心
        '''
        self.driver.find_element_by_link_text(u'供应商中心').click()

        self.driver.find_element_by_link_text(u'供应商信息').click()

        intr = self.driver.find_element_by_id('textarea')
        intr.clear ()                 #??????????????????????????????????????????????????????????????????????????????????????????????
        intr.send_keys(contain)

        self.driver.find_element_by_class_name('confirm_btn_b').submit()

    def modify_portrait(self,pathofportrait):
        '''no pass and finish :modify portrait
        '''
        self.driver.find_element_by_link_text(u'供应商中心').click()

        self.driver.find_element_by_link_text(u'店铺设置').click()

        self.driver.find_element_by_name('upload_image').send_keys(pathofportrait)  #????????????????????????????????????????????????????????????????????

        self.driver.find_element_by_class_name('confirm_btn_b').submit()

    def modify_address(self,name,province,city,area,detail_address,post_code,phone_number,telephone):
        '''no pass and no finish: modify the address for receiving product
        '''
        self.driver.find_element_by_link_text(u'供应商中心').click()

        self.driver.find_element_by_link_text(u'退货地址管理').click()

        name = self.driver.find_element_by_name('name')
        name.clear()
        name.send_keys(name)

        #select the drop down of list about address
        p = self.driver.find_element_by_id('province')
        Select(p).select_by_value(provice)

        c = self.driver.find_element_by_id('city')
        Select(c).select_by_value(city)

        a = self.driver.find_element_by_id('county')
        Select(a).select_by_value(area)

        self.driver.find_element_by_name('address').send_keys(detail_address)

        self.driver.find_element_by_name('postcode').send_keys(post_code)

        self.driver.find_element_by_id('takePhone').send_keys(phone_number)

        self.driver.find_element_by_id('takeTell').send_keys(telephone)

        self.driver.find_element_by_class_name('confirm_btn_b').submit()      

    def logout(self):
        '''log out
        '''
        self.driver.find_element_by_link_text(u'退出登录').click()
        sleep(10)

    
class bloc_services_section(object):
    '''the test of service
    '''
    def login(self):pass

    def add_sub_service(self):pass

    def edit_sub_service(self):pass

    def del_sub_service(self):pass

    def modify_password(self):pass

    def modify_introduct(self):pass

    def modify_set_service(self):pass

    def logout(self):pass



class service_section(object):
    '''the test of the bloc service ,enterprise service and personal service.
    '''
    def login(self):pass

    def verity_code(self):pass

    def add_service(self):pass

    def edit_service(self):pass

    def del_service(self):pass

    def add_distrib_sale_product(self):pass

    def edd_distrib_sale_product(self):pass
     
    def add_trainer(self):pass

    def edit_trainer(self):pass

    def del_trainer(self):pass

    def add_counselor(self):pass

    def edit_counselor(self):pass

    def del_counselor(self):pass

    def modify_password(self):pass

    def modify_introduct(self):pass 

    def modify_set_service(self):pass

    def logout(self):pass