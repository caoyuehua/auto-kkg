#!/usr/bin/env python
#-*- coding=UTF-8 -*-
from selenium.common.exceptions import NoSuchElementException,NoAlertPresentException
from selenium.webdriver.common.by import By

class output:
    '''
    Record the log when execute the autoTest porject
    '''
    @classmethod
    def openfile(cls,filename):
        '''
        create a record file
        '''
        cls.f = file(filename,'a+')

    @classmethod
    def save(cls,content):
        '''
        save the log which should be record
        '''
        cls.f.write(content+'\n')

    @classmethod
    def quit(cls):
       cls.f.close()

class verify:
    '''
    verify of the element or other
    '''

    @classmethod
    def v_element(cls, driver, how, what):
        try: driver.find_element(by=how, value=what)
        except NoSuchElementException, e:return False
        return True

    @classmethod
    def v_alert(cls,driver):
        try: driver.switch_to_alert()
        except NoAlertPresentException, e: return False
        return True